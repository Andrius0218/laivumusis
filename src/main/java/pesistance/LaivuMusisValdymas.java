package pesistance;

public interface LaivuMusisValdymas {


//    pirmas zaidimo zingsnis

//    @param zaidejoVardas prisijungusio zmogaus vardas

    void ivestiVarda(String zaidejoVardas);

    void padetiLaiva(LaivoDydis dydis, int x, int y, Kryptis kryptis);


    LangelioReiksme[][] parodytiManoLenta();

    LangelioReiksme[][] parodytiPriesininkoLenta();
}
