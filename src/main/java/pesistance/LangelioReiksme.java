package pesistance;

public enum LangelioReiksme {
    PASAUTAS,
    NUSKANDINTAS,
    BANDYMAS,
    JURA,
    LAIVAS;
}
