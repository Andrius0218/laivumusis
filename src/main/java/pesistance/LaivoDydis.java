package pesistance;

public enum LaivoDydis {
    KETURVIETIS(1),
    TRIVIETIS(2),
    DVIVIETIS(3),
    VIENVIETIS(4);

    public final int kiekis;

    LaivoDydis(int kiekis){
        this.kiekis = kiekis;
    }

}
